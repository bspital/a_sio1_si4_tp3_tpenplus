package tp4;

import java.util.Random;
import java.util.Scanner;

public class TP4deMulti {

    public static void main(String[] args) {
        Scanner saisie = new Scanner(System.in);
        Random rd = new Random();
        int nbralea, stop = 0, total = 0, coup = 0, o_nbralea, o_total = 0, o_coup = 0;
        String next;
        
        System.out.println("Le but du jeu est d'obtenir un total de 20 en lançant plusieurs fois un dé avant que l'ordinateur n'y arrive avant vous.");
        while(stop == 0) {
            nbralea = rd.nextInt(6)+1;
            o_nbralea = rd.nextInt(6)+1;
            System.out.println("Le dé a donné le chiffre " + nbralea + " pour vous et "+ o_nbralea +" pour l'ordinateur");
            coup++;
            o_coup++;
            
            if(nbralea + total > 20) {
                total = total - nbralea;
                System.out.println("Le total est supérieur à 20 donc on soustrait le " + nbralea + " du dé au total antérieur.");
            }
            else {
                total = total + nbralea;
            }
            
            if(o_nbralea + o_total > 20) {
                o_total = o_total - o_nbralea;
                System.out.println("Le total de l'ordinateur est supérieur à 20 donc on soustrait le " + o_nbralea + " du dé au total antérieur.");
            }
            else {
                o_total = o_total + o_nbralea;
            }
            
            if(total == 20 || o_total == 20) {
                System.out.println("Partie terminée !");
                if(total == 20) {
                    System.out.println("Vous avez gagné ! Le total de 20 est atteint en " + coup + " essais.");
                }
                else if(o_total == 20) {
                    System.out.println("Vous avez perdu ! Le total de 20 pour l'ordinateur est atteint en " + o_coup + " essais.");
                }
                       
                stop = 1;
            }
            else {
                System.out.println("Le total actuel est de " + total + " pour vous et de "+ o_total +" pour l'ordinateur. Entrez un chiffre et appuyez sur entrée pour continuer.");
                next = saisie.next();
            }
        }
    }
}
