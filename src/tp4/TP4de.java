package tp4;

import java.util.Random;
import java.util.Scanner;

public class TP4de {

    public static void main(String[] args) {
        Scanner saisie = new Scanner(System.in);
        Random rd = new Random();
        int nbralea, stop = 0, total = 0, coup = 0;
        String next;
        
        System.out.println("Le but du jeu est d'obtenir un total de 20 en lançant plusieurs fois un dé.");
        while(stop == 0) {
            nbralea = rd.nextInt(6)+1;
            System.out.println("Le dé a donné le chiffre " + nbralea);
            coup++;
            if(nbralea + total > 20) {
                total = total - nbralea;
                System.out.println("Le total est supérieur à 20 donc on soustrait le " + nbralea + " du dé au total antérieur.");
            }
            else {
                total = total + nbralea;
            }
            if(total == 20) {
                System.out.println("Partie terminée ! Le total de 20 est atteint en " + coup + " essais.");
                stop = 1;
            }
            else {
                System.out.println("Le total actuel est de " + total + ". Entrez un chiffre et appuyez sur entrée pour continuer.");
                next = saisie.next();
            }
        }
    }
}
